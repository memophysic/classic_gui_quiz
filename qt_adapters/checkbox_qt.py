from collections import OrderedDict

from PyQt5.QtWidgets import QCheckBox, QMainWindow

from definitions.options_selector import BaseOptionsSelector
from qt_adapters.view_qt import ViewQt


class CheckboxQt(BaseOptionsSelector, QCheckBox):

    def __init__(self):
        QCheckBox.__init__(self)
        BaseOptionsSelector.__init__(self)

        self._base_text = ""
        self.stateChanged.connect(self._update_content)

    def value(self):
        options = list(self._options.items())
        return options[0][1] if self.isChecked() else options[1][1]

    def text(self):
        return self._base_text

    def set_text(self, text):
        self._base_text = text
        self._update_content()

    def set_options(self, options):
        super().set_options(options)
        self._update_content()

    def set_selection(self, value):
        self.setChecked(value == list(self.options().values())[0])
        #  Automatically calls _update_content through stateChanged signal

    def _update_content(self):
        if not self.options():
            return

        options = list(self.options().items())
        QCheckBox.setText(self, "{} (Selected: {})".format(
            self._base_text,
            options[0][0] if self.isChecked() else
            options[1][0]
        ))

if __name__ == "__main__":
    opts = OrderedDict()
    opts["Yes"] = 1.0
    opts["No"] = -1.0
    opts["Mostly Yes"] = 0.5
    opts["Mostly No"] = -0.5
    opts["50/50"] = 0.0

    view = ViewQt(QMainWindow)

    checkbox = CheckboxQt()
    checkbox.set_options(opts)
    checkbox.set_text("Test checkbox")
    view.main_window.setCentralWidget(checkbox)

    print("Default value: {}".format(checkbox.value()))

    view.launch()
