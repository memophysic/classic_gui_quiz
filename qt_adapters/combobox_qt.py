from collections import OrderedDict

from PyQt5.QtWidgets import QComboBox, QMainWindow, QWidget, QHBoxLayout, QLabel

from definitions.options_selector import BaseOptionsSelector
from qt_adapters.view_qt import ViewQt


class ComboboxQt(BaseOptionsSelector, QWidget):

    def __init__(self):
        QWidget.__init__(self)
        BaseOptionsSelector.__init__(self)

        layout = QHBoxLayout()
        self.setLayout(layout)

        self._label = QLabel(self)
        layout.addWidget(self._label)

        self._combobox = QComboBox(self)
        layout.addWidget(self._combobox)

    def value(self):
        return self._options[self._combobox.currentText()]

    def set_selection(self, value):
        options = list(self.options().values())
        if value in options:
            self._combobox.setCurrentIndex(options.index(value))

    def text(self):
        return self._label.text()

    def set_text(self, text):
        self._label.setText(text)

    def set_options(self, options):
        if self.options() == options:
            return

        super().set_options(options)
        self._combobox.clear()
        self._combobox.addItems(options.keys())

if __name__ == "__main__":
    opts = OrderedDict()
    opts["Yes"] = 1.0
    opts["No"] = -1.0
    opts["Mostly Yes"] = 0.5
    opts["Mostly No"] = -0.5
    opts["50/50"] = 0.0

    view = ViewQt(QMainWindow)

    combobox = ComboboxQt()
    combobox.set_options(opts)
    combobox.set_text("Test checkbox")
    view.main_window.setCentralWidget(combobox)

    print("Default value: {}".format(combobox.value()))

    view.launch()
