import os
import sys

from PyQt5 import uic
from PyQt5.QtWidgets import QMainWindow, QApplication

from definitions.view import View
from utils import project_root


def qt_type_from_ui(ui_file):
    """
    Wrapper around the Qt type loader for UI file, allowing to load directly
    a *.ui file and ensuring the QApplication exist (since it is needed)
    :param ui_file: File to load from the ui folder
    :return: Ui file's main class
    """
    ViewQt.initialize_application()
    return uic.loadUiType(os.path.join(project_root(), "ui", ui_file))[0]


class ViewQt(View):
    """
    Main view of the application
    """

    QT_APPLICATION = None

    @classmethod
    def initialize_application(cls):
        if not ViewQt.QT_APPLICATION:
            ViewQt.QT_APPLICATION = QApplication([])

    def __init__(self, main_window_factory=QMainWindow):
        self.initialize_application()
        self.main_window = main_window_factory()

    def __del__(self):
        self.exit()

    def launch(self):
        self.main_window.show()
        sys.exit(ViewQt.QT_APPLICATION.exec_())

    def exit(self):
        self.main_window.close()
