import random
from collections import OrderedDict
from itertools import zip_longest

from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QWidget, QMainWindow, QLayout, QMessageBox, \
    QCheckBox, QHBoxLayout, QAbstractButton

from model.polarization import compute_polarization, Polarization
from qt_adapters.checkbox_qt import CheckboxQt
from qt_adapters.combobox_qt import ComboboxQt
from qt_adapters.radiobox_qt import RadioboxQt
from qt_adapters.view_qt import ViewQt, qt_type_from_ui
from definitions.options_selector import OptionsSelector, \
    OptionNotSelectedException


class QuizMainWindow(QMainWindow, qt_type_from_ui("quiz_main_window.ui")):
    OPTIONS = OrderedDict()
    OPTIONS["Yes"] = 1.0
    OPTIONS["No"] = -1.0
    OPTIONS["Mostly Yes"] = 0.5
    OPTIONS["Mostly No"] = -0.5
    OPTIONS["50/50"] = 0.0

    QUESTIONS_TEXT = [
        "Do you like cats?",
        "Are you usually the driver when in a car?",
        "Do you prefer Star Wars to Star Trek?",
        "Do you prefer web to TV?",
        "Do you prefer summer to winter?"
    ]

    TIMER_DELAY = 1000

    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        self.setupUi(self)

        self.startButton.clicked.connect(self.on_start)
        self.submitButton.clicked.connect(self.on_submit)

        self._question_widgets = {
            self.question1,
            self.question2,
            self.question3,
            self.question4,
            self.question5
        }
        """:type: list[QWidget]"""

        self._candidates = {
            CheckboxQt: [],
            RadioboxQt: [],
            ComboboxQt: []
        }

        self.initialize_widgets()

        self.update_option_selectors()

        self._timer = QTimer()
        self._timer.timeout.connect(self.update_option_selectors)
        self._timer.start(QuizMainWindow.TIMER_DELAY)

        self._quiz_started = False

    def on_start(self):
        """
        Stop the monitoring of the polarization to freeze the components and
        start the quiz
        """
        self._timer.stop()
        self._quiz_started = True
        self.startButton.setEnabled(False)

    def on_submit(self):
        if not self._quiz_started:
            QMessageBox(QMessageBox.Warning,
                        "Quiz not started",
                        "Please start the quiz prior to submitting "
                        "responses, thank you!").exec_()
            return

        results = {}
        unanswered = []

        for widget in self._question_widgets:
            option_selector = self._get_first_widget_child(widget)
            try:
                results[option_selector.text()] = option_selector.value()
            except OptionNotSelectedException:
                unanswered.append(option_selector.text())

        if unanswered:
            error_msg = "ERROR! The following questions were not answered:\n\n"
            for question in unanswered:
                error_msg += "'{}'\n".format(question)
            QMessageBox(QMessageBox.Warning,
                        "Error in submitting",
                        error_msg).exec_()
        else:
            # TODO: Save in database
            QMessageBox(QMessageBox.Warning,
                        "Submit completed",
                        "Submit completed, thank you!").exec_()

            self._timer.start(QuizMainWindow.TIMER_DELAY)
            self._quiz_started = False
            self.startButton.setEnabled(True)

    def initialize_widgets(self):
        for q in self._question_widgets:
            q.setLayout(QHBoxLayout())

    def set_option_selector_components(self, component):
        """
        :param component: Type of component to set
        """
        assert component in self._candidates

        for q, w, c, i in zip_longest(
                QuizMainWindow.QUESTIONS_TEXT,
                self._question_widgets,
                self._candidates[component],
                range(0, len(QuizMainWindow.QUESTIONS_TEXT)),
                fillvalue=None):
            l = w.layout()
            """:type: QLayout"""

            if c is None:
                c = component()
                c.set_options(QuizMainWindow.OPTIONS)
                c.set_text(q)
                self._candidates[component].append(c)

            old_component = self._get_first_widget_child(w)

            if old_component:
                l.removeWidget(old_component)
                old_component.setParent(None)
                c.state_transfer(old_component)

            l.addWidget(c)

    @staticmethod
    def _get_first_widget_child(widget):
        children = [child for child in widget.children()
                    if not isinstance(child, QLayout)]
        return children[0] if children else None


    def update_option_selectors(self):
        option_selector_type = {
            Polarization.low: CheckboxQt,
            Polarization.medium: RadioboxQt,
            Polarization.high: ComboboxQt
        }.get(compute_polarization()) or CheckboxQt

        self.set_option_selector_components(option_selector_type)


if __name__ == "__main__":
    view = ViewQt(QuizMainWindow)
    view.launch()
