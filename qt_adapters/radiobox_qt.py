from collections import OrderedDict

from PyQt5.QtWidgets import QButtonGroup, QRadioButton, QMainWindow, \
    QGroupBox, QHBoxLayout

from definitions.options_selector import OptionNotSelectedException, \
    BaseOptionsSelector
from qt_adapters.view_qt import ViewQt


class RadioButtonQtWithValue(QRadioButton):
    def __init__(self, text, value):
        super().__init__(text)
        self._value = value

    def value(self):
        return self._value


class RadioboxQt(BaseOptionsSelector, QGroupBox):

    def __init__(self):
        QGroupBox.__init__(self)
        BaseOptionsSelector.__init__(self)

        self.setLayout(QHBoxLayout())
        self._button_group = QButtonGroup(self)

    def _clear(self):
        l = self.layout()
        """:type: QLayout"""
        for b in self._button_group.buttons():
            self._button_group.removeButton(b)
            l.removeWidget(b)
            b.setParent(None)
            b.deleteLater()

    def _add_button(self, text, value, checked=False):
        button = RadioButtonQtWithValue(text, value)
        button.setChecked(checked)
        self._button_group.addButton(button)
        self.layout().addWidget(button)
        button.setParent(self)

    def value(self):
        button = self._button_group.checkedButton()
        """:type: RadioButtonQtWithValue"""
        if button is None:
            raise OptionNotSelectedException()
        return button.value()

    def text(self):
        return self.title()

    def set_text(self, text):
        self.setTitle(text)

    def set_options(self, options):
        if self._options == options:
            return
        super().set_options(options)
        self._update_option_view()

    def set_selection(self, value):
        options = list(self.options().values())
        if value in options:
            buttons = self._button_group.buttons()
            """:type: list[RadioButtonQtWithValue]"""
            button = next((b for b in buttons if b.value() == value), None)
            if button:
                button.setChecked(True)

    def _update_option_view(self):
        """
        Update the options based on polarization, including state transfer if
        possible
        """
        options = list(self.options().items())
        if not options:
            return

        try:
            previous_value = self.value()
        except OptionNotSelectedException:
            previous_value = None

        self._clear()
        for i in range(0, min(len(options), 4)):
            text, value = options[i]
            self._add_button(text, value, checked=previous_value == value)


if __name__ == "__main__":
    opts = OrderedDict()
    opts["Yes"] = 1.0
    opts["No"] = -1.0
    opts["Mostly Yes"] = 0.5
    opts["Mostly No"] = -0.5
    opts["50/50"] = 0.0

    view = ViewQt(QMainWindow)

    radiobox = RadioboxQt()
    radiobox.set_options(opts)
    radiobox.set_text("Test radiobox")
    radiobox.set_selection(1.0)
    view.main_window.setCentralWidget(radiobox)

    print("Default value: {}".format(radiobox.value()))

    view.launch()
