import random
from enum import Enum


class Polarization(Enum):
    low = 0
    medium = 1
    high = 2


def compute_polarization():
    """
    :rtype: Polarization
    """
    # Would retrieve values from DB, but simulate through random

    # noinspection PyTypeChecker
    return random.choice(list(Polarization))
