import os
import sys


def project_root():
    if getattr(sys, 'frozen', False):
        # If the app is bundled, get the directory (using PyInstaller)
        return sys._MEIPASS
    else:
        # Support both cases if the file was run directly or by the module
        # folder
        return os.path.dirname(__file__) if __file__[-3:] == ".py" else __file__