class View:
    """
    Responsible for displaying the application graphical interface
    """
    def launch(self):
        """
        Request the view to display the application's graphical interface
        """
        raise NotImplementedError()

    def exit(self):
        """
        Exit the view and set its components as ready for deletion
        """
        raise NotImplementedError()
