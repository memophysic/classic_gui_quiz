from collections import OrderedDict


class OptionNotSelectedException(Exception):
    def __init__(self, msg=None):
        super(Exception, self).__init__(msg)
        self._msg = msg

    def __str__(self):
        return self._msg


class OptionsSelector:
    def set_text(self, text):
        """
        :param text: Text describing the nature of the option selection
        """
        raise NotImplementedError()

    def text(self):
        """
        :return: Text describing the nature of the option selection
        """
        raise NotImplementedError()

    def set_options(self, options):
        """
        :param options: Options as string and a value in range [-1, 1]
        :type options: OrderedDict
        """
        raise NotImplementedError()

    def options(self):
        """
        :rtype: OrderedDict
        """
        raise NotImplementedError()

    def value(self):
        """
        :return: Value in range [-1, 1] the currently selected option represents
        :raises: OptionNotSelectedException
        """
        raise NotImplementedError()

    def set_selection(self, value):
        """
        Set the active selection corresponding to the passed value
        If the value is not handled by the component, it retains its current
        selection
        :param value: Value in the range [-1, 1]
        """
        raise NotImplementedError()

    def state_transfer(self, source):
        """
        :type source: OptionsSelector
        """
        self.set_text(source.text())
        self.set_options(source.options())
        try:
            value = source.value()
            self.set_selection(value)
        except OptionNotSelectedException:
            pass  # No selection done, no selection state to transfer


class BaseOptionsSelector(OptionsSelector):
    def __init__(self):
        self._options = {}

    def set_options(self, options):
        """
        :type options: OrderedDict
        """
        self._options = options

    def options(self):
        return self._options
